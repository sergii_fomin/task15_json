package com.fomin.services;

import com.fomin.model.Flower;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class JacksonParser {

    private ObjectMapper objectMapper = new ObjectMapper();
    public Flower[] parseFlowers(String jsonFilePath) throws IOException {
        return objectMapper.readValue(new File(jsonFilePath), Flower[].class);
    }
}
