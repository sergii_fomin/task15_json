package com.fomin.services;

import com.fomin.model.Flower;

import java.util.Comparator;

public class FlowerComparator implements Comparator<Flower> {
    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.getName().length() - o2.getName().length();
    }
}
