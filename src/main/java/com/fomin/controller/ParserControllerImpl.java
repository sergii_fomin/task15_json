package com.fomin.controller;

import com.fomin.services.GsonParser;
import com.fomin.services.JacksonParser;
import com.fomin.validation.JsonValidator;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.IOException;
import java.util.Arrays;

public class ParserControllerImpl implements ParserController {
    private GsonParser gsonParser = new GsonParser();

    private JacksonParser jacksonParser = new JacksonParser();

    private String jsonFilePath = "d:/Epam/Practice/Fomin_Sergii/task15_json/src/main/resources/gallery.json";
    private String jsonSchemaPath = "d:/Epam/Practice/Fomin_Sergii/task15_json/src/main/resources/gallerySchema.json";

    @Override
    public void printParsedJson() {
        try {
            if (!validateJson()) {
                System.out.println("Invalid json file");
            } else {
                System.out.println("Gson parser:");
                Arrays.asList(gsonParser.parseFlowers(jsonFilePath))
                        .forEach(System.out::println);
                System.out.println("Jackson parser:");
                Arrays.asList(jacksonParser.parseFlowers(jsonFilePath))
                        .forEach(System.out::println);
            }
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
    }

    private boolean validateJson() throws IOException, ProcessingException {

        return JsonValidator.isValid(jsonFilePath, jsonSchemaPath);
    }
}
