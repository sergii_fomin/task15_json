package com.fomin.controller;

import com.fomin.model.Flower;
import com.fomin.services.FlowerComparator;
import com.fomin.services.JacksonParser;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SortControllerImpl implements SortController {
    @Override
    public void printSortedData() {
        String jsonFilePath = "d:/Epam/Practice/Fomin_Sergii/task15_json/src/main/resources/gallery.json";
        JacksonParser parser = new JacksonParser();
        try {
            List<Flower> flowers = Arrays.asList(parser.parseFlowers(jsonFilePath));
            flowers.sort(new FlowerComparator());
            flowers.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
